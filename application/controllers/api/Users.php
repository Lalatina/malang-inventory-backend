<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Users extends RestController {

    private $table = 'users';

    function __construct()
    {
        parent::__construct();
    }

    # resource users function
    public function index_get()
    {
        $this->response([
            'status' =>  true,
            'message' => 'Url Not Found',
        ],200);
    }

    public function list_users_get()
    {
        $query = $this->db->query('SELECT * FROM users ORDER BY created_at DESC')->result();
        if ($query) {
            $this->response([
                'status' =>  true,
                'message' => 'Data Found',
                'data' => $query
            ],200);
        }else{
            $this->response([
                'status' =>  false,
                'message' => 'Data Not Found',
                'data' => []
            ],400);            
        }
    }

    public function users_create_post()
    {
        $configFormValidation = array(
            array(
                'field' => 'nama',
                'label' => 'nama',
                'rules' => 'required',                               
            ),
            array(
                'field' => 'email',
                'label' => 'email',
                'rules' => 'trim|required|valid_email',
                ),
            array(
                'field' => 'password',
                'label' => 'password',
                'rules' => 'required',
                ),             
        );
        $this->form_validation->set_rules($configFormValidation);
        if($this->form_validation->run()){
            $postData = new StdClass();
            $postData->nama = $this->input->post('nama');
            $postData->email = $this->input->post('email');
            $postData->id_roles = 1;
            $postData->password = md5($this->input->post('password'));
            $randomApiKey = substr(str_replace(['+', '/', '='], '', base64_encode(random_bytes(32))), 0, 32);
            $postData->api_key = password_hash($this->input->post('password').'/=-/.{{\\{{\\}'.$randomApiKey,PASSWORD_BCRYPT);
            $query = $this->db->insert($this->table, $postData);
            if ($query) {
                $this->response([
                    'status' => true,
                    'message' => 'success',
                    'data' => []
                ], 200); 
            } else {
                $this->response([
                    'status' => false,
                    'message' => 'error',
                    'data' => []
                ], 400); 
            }
        } else{
            $validation = $this->form_validation->error_array();
            $field = array();
                     foreach($validation as $key => $value) {
                        $field[] = $value;
                     }
            
            $this->response([
                'status' => false,
                'message' => $field,
                'data' => []
            ], 400);
        }

    }


    public function users_edit_post()
    {
        $id = $this->input->post('id');
        $query = $this->db->query("SELECT * FROM users WHERE id='$id'")->row();
        if ($query) {
            $this->response([
                'status' => true,
                'message' => 'Data Found',
                'data' => $query
            ], 200); 
        } else {
            $this->response([
                'status' => false,
                'message' => 'Data Not Found',
                'data' => []
            ], 400); 
        }

    }

    public function users_update_post()
    {
        $configFormValidation = array(
            array(
                'field' => 'id',
                'label' => 'id',
                'rules' => 'required',                               
            ),            
            array(
                'field' => 'nama',
                'label' => 'nama',
                'rules' => 'required',                               
            ),
            array(
                'field' => 'email',
                'label' => 'email',
                'rules' => 'trim|required|valid_email',
                )            
        );
        $this->form_validation->set_rules($configFormValidation);
        if($this->form_validation->run()){
            $postData = new StdClass();
            $postData->nama = $this->input->post('nama');
            $postData->email = $this->input->post('email');
            $this->db->where('id', $this->input->post('id'));
            $query = $this->db->update($this->table,$postData);
            if ($query) {
                $this->response([
                    'status' => true,
                    'message' => 'success',
                    'data' => []
                ], 200); 
            } else {
                $this->response([
                    'status' => false,
                    'message' => 'error',
                    'data' => []
                ], 400); 
            }
        } else{
            $this->response([
                'status' => false,
                'message' => $this->form_validation->error_array(),
                'data' => []
            ], 400);
        }

    }

    public function users_delete_post()
    {
        $this->db->where('id', $this->input->post('id'));
        $this->db->delete($this->table);
        if ($this->db->affected_rows()) {
            $this->response([
                'status' => true,
                'message' => 'Data Found',
                'data' => []
            ], 200); 
        } else {
            $this->response([
                'status' => false,
                'message' => 'Data Not Found',
                'data' => []
            ], 400); 
        }

    }    
    # resource users function

}