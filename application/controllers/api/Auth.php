<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE');
class Auth extends CI_Controller {

    private $table = 'users';
    
    function __construct()
    {

        parent::__construct();
    }
  
    

    public function index()
    {
        $guard_token = $this->input->post('guard_token');
        $query = $this->db->query("SELECT id_roles,api_key FROM users WHERE api_key='$guard_token'")->row();
        if ($query) {
            return response([
                'status' => true,
                'message' => 'success',
                'data' => $query,
            ], 200); 
        }else{
            return response([
                'status' => false,
                'message' => 'error',
                'data' => []
            ], 400);             
        }        
    }

    public function login()
    {

        $email = $this->input->post('email');
        $password = md5($this->input->post('password'));
        $query = $this->db->query("SELECT id_roles,api_key FROM users WHERE email='$email' && password='$password'")->row();
        if ($query) {
            return response([
                'status' => true,
                'message' => 'success',
                'data' => [
                    'guard' => $query->id_roles,
                    'guard_token' => $query->api_key
                ],
            ], 200); 
        }else{
            return response([
                'status' => false,
                'message' => 'error',
                'data' => []
            ], 400);             
        }

    }
    
    public function register()
    {
        $configFormValidation = array(
            array(
                'field' => 'nama',
                'label' => 'nama',
                'rules' => 'required',                               
            ),
            array(
                'field' => 'email',
                'label' => 'email',
                'rules' => 'trim|required|valid_email',
                ),
            array(
                'field' => 'password',
                'label' => 'password',
                'rules' => 'required',
                ),             
        );
        $this->form_validation->set_rules($configFormValidation);
        if($this->form_validation->run()){        
        $postData = new StdClass();
        $postData->nama = $this->input->post('nama');
        $postData->email = $this->input->post('email');
        $postData->id_roles = 1;
        $postData->password = md5($this->input->post('password'));
        $randomApiKey = substr(str_replace(['+', '/', '='], '', base64_encode(random_bytes(32))), 0, 32);
        $postData->api_key = password_hash($this->input->post('password').'/=-/.{{\\{{\\}'.$randomApiKey,PASSWORD_BCRYPT);
		$insert = $this->db->insert($this->table, $postData);
		if ($insert) {
            return response([
                'status' => true,
                'message' => 'success',
                'data' => $insert
            ], 200); 
		} else {
            return response([
                'status' => false,
                'message' => 'error',
                'data' => []
            ], 404); 
        }
    } else{
        return response([
            'status' => false,
            'message' => $this->form_validation->error_array(),
            'data' => []
        ], 404); 
    }    
    }
}